//
//  VideoLauncher.swift
//  GeoTwitter
//
//  Created by Ahmet Akdeniz on 2019-10-04.
//  Copyright © 2019 aca. All rights reserved.
//

import UIKit
import AVKit


class VideoLauncher: NSObject {
    func playMedia(with urlString: String, tweetImageView: UIImageView){
        if let url = URL(string: urlString){
        URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in

            if error != nil{
                print(error!)
            return
            }

    DispatchQueue.main.async {
        let  player  = AVPlayer(url: url)
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = tweetImageView.bounds
        tweetImageView.layer.addSublayer(playerLayer)
        tweetImageView.roundedCorners()
        player.play()
            }
                }).resume()
        }
    }
}
