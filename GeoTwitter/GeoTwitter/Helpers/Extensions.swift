//
//  Extensions.swift
//  GeoTwitter
//
//  Created by Ahmet Akdeniz on 2019-09-28.
//  Copyright © 2019 aca. All rights reserved.
//

import UIKit
import AVFoundation

let imageCache = NSCache<AnyObject, AnyObject>()

extension UIImageView {
    func loadImageUsingUrlString(urlString:String?){
        
        if  urlString != nil && !urlString!.isEmpty && urlString != ""{
            let url = URL(string: urlString!)
        //Check cache for image first
        image = nil

        if let cachedImage = imageCache.object(forKey: urlString as AnyObject) as? UIImage{
            self.image = cachedImage
            return
        }
        //Otherwise new download

    URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in

    if error != nil{
        print(error!)
    return
    }

    DispatchQueue.main.async {
        if let imageToCache = UIImage(data: data!){
            imageCache.setObject(imageToCache, forKey: ((urlString as AnyObject)))
            self.image = imageToCache
        }
    }
        }).resume()
        }}
}

 extension UIImageView {
    func roundedCorners(){
        self.layer.cornerRadius = 20.0
        layer.masksToBounds = true
    }
}
