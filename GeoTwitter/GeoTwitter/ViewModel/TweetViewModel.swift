//
//  TweetViewModel.swift
//  GeoTwitter
//
//  Created by Ahmet Akdeniz on 2019-10-10.
//  Copyright © 2019 aca. All rights reserved.
//

import Foundation
import UIKit

// MVC to MVVM 
struct TweetViewModel {
    let id: String
    let username: String
    let screenName: String
    let text: String
    let createdAt: String?
    let profileImageUrlString : String?
    let mediaUrlString: String?
    let latitude: Double?
    let longitude: Double?
    let location: String?
    
    init(tweet: Tweet){
        self.id = tweet.id
        self.username = tweet.username
        self.screenName = tweet.screenName
        self.text = tweet.text
        self.createdAt = tweet.createdAt
        self.profileImageUrlString = tweet.profileImageUrlString
        self.mediaUrlString = tweet.mediaUrlString
        self.latitude = tweet.latitude
        self.longitude = tweet.longitude
        self.location = tweet.location
    }
}
