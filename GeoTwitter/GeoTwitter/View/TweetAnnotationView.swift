//
//  TweetAnnotationView.swift
//  GeoTwitter
//
//  Created by Ahmet Akdeniz on 2019-09-28.
//  Copyright © 2019 aca. All rights reserved.
//

import MapKit

class TweetAnnotationView: MKAnnotationView {
    override var annotation: MKAnnotation? {
        willSet {
            guard let tweetAnnotation = newValue as? TweetAnnotation else {return}
            canShowCallout = true
            calloutOffset = CGPoint(x: -5, y: 5)
   
            let detailButton = UIButton(frame:CGRect(origin: CGPoint.zero, size: CGSize(width: 30, height: 30)))
            detailButton.setBackgroundImage(UIImage(named: "info_icon"), for: UIControl.State())
            detailButton.addTarget(MapViewController().self, action: #selector(MapViewController().detailButtonTapped), for: .touchUpInside)

            rightCalloutAccessoryView = detailButton
 
            if let imageName = tweetAnnotation.imageName {
                    image = UIImage(named: imageName)
            } else {
                image = nil
            }
        }
    }
}
