//
//  TweetTableViewCell.swift
//  GeoTwitter
//
//  Created by Ahmet Akdeniz on 2019-09-28.
//  Copyright © 2019 aca. All rights reserved.
//
@objc protocol TweetCellDelegate: class {
    
    // Passing delegator (current object) to delegatee (Table View)
    // Our cell wants to know its indexPath, but only TableView has this ability NOT the cell itself, so
    // cell delegats this TableView
    // Delegator (In this case our selected cell) pass inself to delegatee, so
    // Delegatee can find out delegator indexPath
    func imageViewTapped(sender: TweetTableViewCell)
    var imageViewUrl: String {get set}
}

import UIKit
import AVKit
import AVFoundation

class TweetTableViewCell: UITableViewCell {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var tweetTextLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var screenNameLabel: UILabel!
    @IBOutlet weak var tweetImageView: UIImageView!
    @IBOutlet weak var retweetButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var createdAtLabel: UILabel!
    
    var delegate: TweetCellDelegate?
    
      var tweet: Tweet?
    
       func update (with tweet: Tweet?) {

        let profileImageUrl = tweet?.profileImageUrlString ?? "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png"
        profileImageView.loadImageUsingUrlString(urlString: profileImageUrl)
        profileImageView.roundedCorners()
       
        if let tweetUrl = tweet?.mediaUrlString {
            let videoLauncher = VideoLauncher()
            videoLauncher.playMedia(with: tweetUrl, tweetImageView: tweetImageView)
            delegate?.imageViewUrl = tweetUrl
            tweetImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleSelectedImageView)))
            tweetImageView.isUserInteractionEnabled = true
        }else{
            tweetImageView.image = nil
            defaultTweetImageViewHeightConstraint = tweetImageViewHeightConstraint.constant
            tweetImageViewHeightConstraint.constant = 0
            layoutIfNeeded()
        }

        usernameLabel.text = tweet?.username ?? ""
        screenNameLabel.text = tweet?.screenName ?? ""
        tweetTextLabel.text = tweet?.text ?? ""
        if let tweet = tweet {
        if let created = tweet.createdAt{
            let formatter = DateFormatter()
        if let date = formatter.date(from: created){
        if Date().timeIntervalSince(date)>24*60*60 {
                formatter.dateStyle = .short
                
            }else {
                formatter.dateStyle = .short
            }
                let createdText = formatter.string(from: date)
            createdAtLabel.text = createdText
            }
        } else {
            createdAtLabel.text = tweet.createdAt
        }
        }else {
            createdAtLabel.text = ""
        }
    }
    
    @IBOutlet weak var tweetImageViewHeightConstraint: NSLayoutConstraint!
    private var defaultTweetImageViewHeightConstraint: CGFloat!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        if(tweetImageViewHeightConstraint != nil && defaultTweetImageViewHeightConstraint != nil){
            tweetImageViewHeightConstraint.constant = defaultTweetImageViewHeightConstraint
        }
    }
    
    @objc func handleSelectedImageView(){
        delegate?.imageViewTapped(sender: self)
    }
}
