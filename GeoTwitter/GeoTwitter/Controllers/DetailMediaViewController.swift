//
//  DetailMediaViewController.swift
//  GeoTwitter
//
//  Created by Ahmet Akdeniz on 2019-10-10.
//  Copyright © 2019 aca. All rights reserved.
//

import UIKit

class DetailMediaViewController: UIViewController {
    
    @IBOutlet weak var mediaImageView: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var url: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        
        if let url = url{
        let videoLauncher = VideoLauncher()
        videoLauncher.playMedia(with: url, tweetImageView: mediaImageView)
        activityIndicator.isHidden = true
        activityIndicator.stopAnimating()
        }
    }
}
