//
//  TweetsViewController.swift
//  GeoTwitter
//
//  Created by Ahmet Akdeniz on 2019-09-28.
//  Copyright © 2019 aca. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class TweetsViewController: UIViewController, TweetCellDelegate{
    var imageViewUrl: String = ""
    
    var searchForText: String?
    var isSearching = false
    var tweetList : [Tweet] = []

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    let searchController = UISearchController(searchResultsController: nil)
   
    static let cellId = "tweetCell"
      
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        searchBar.delegate = self
    
        tableView.estimatedRowHeight = tableView.rowHeight
        tableView.rowHeight = UITableView.automaticDimension
        
        //Default search text
        searchForText = "Messi"
        searchForTweets(cc:  oauth1.cc, uc:oauth2.uc)
    }
    
    private func getHomeTimeline(cc : (String, String), uc : (String, String)){
        let url = URL(string: "https://api.twitter.com/1.1/statuses/home_timeline.json")!
        let params = ["count":"10"]
        let timestamp = String(Int(Date().timeIntervalSince1970))
        let nonce =  UUID().uuidString
    
        let signature = OAuth.calculateSignature(url: url, method: "GET",parameter: params, timestamp: timestamp, nonce: nonce, consumerCredentials: cc, userCredentials: uc )
                    
        let headers = ["Authorization": signature]
            
            Alamofire.request(url, method:.get, parameters: params, headers: headers).responseJSON (completionHandler: { response in switch response.result {
                case .success(let value):
                let json = JSON(value)
                var i = 0
                while (i < 10){
                let data = json[i]
                
                let id = data["id_str"].stringValue
                let text = data["text"].stringValue
                let createdAt = data["created_at"].stringValue
                let user = data["user"]
                let username = user["name"].stringValue
                let screenName = user["screen_name"].stringValue
                let profileImageUrlString = user["profile_image_url"].stringValue
                let urlString: String = profileImageUrlString
                    
                let mediaUrlString:String? = user["entities"]["description"]["urls"][0]["expanded_url"].string
                
                let tweet = Tweet(id: id, username: username, screenName: screenName, text: text, createdAt: createdAt, profileImageUrlString: urlString, mediaUrlString: mediaUrlString, latitude: nil, longitude: nil, location: nil )
                self.tweetList.append(tweet)

                    i+=1
                }
                self.tableView.reloadData()
                
            case .failure(let error):
                print("Request failed with error: \(error)")
                        }
            }) 
    }
    
    private func searchForTweets(cc : (String, String), uc : (String, String)){
        let searchFor = self.searchForText!

          let url = URL(string: "https://api.twitter.com/1.1/search/tweets.json")!
          let params = ["count":"10", "q":searchFor]
            
          let timestamp = String(Int(Date().timeIntervalSince1970))
          let nonce =  UUID().uuidString
          let signature = OAuth.calculateSignature(url: url, method: "GET",parameter: params, timestamp: timestamp, nonce: nonce, consumerCredentials: cc, userCredentials: uc )
                        
            let headers = ["Authorization": signature]
                
                Alamofire.request(url, method:.get, parameters: params, headers: headers).responseJSON (completionHandler: { response in switch response.result {
                    case .success(let value):
                    let json = JSON(value)
                    var i = 0
                    while (i < 10){
                    let data = json["statuses"]
                    let text = data[i]["text"].stringValue
              
                    let mediaUrlString = data[i]["extended_entities"]["media"][0]["video_info"]["variants"][1]["url"].string
                        
                    let expandedUrlString = data[i]["extended_entities"]["media"][0]["media_url"].string
                 
                    let createdAt = data[i]["created_at"].stringValue
                    let user = data[i]["user"]
    
                    let id = user["id"].stringValue
                    let username = user["name"].stringValue
                    let screenName = user["screen_name"].stringValue
                    let profileImageUrlString = user["profile_image_url"].stringValue
                    let urlString: String? = profileImageUrlString
                        
                    let userLocation = user["location"].stringValue
                      
                    let tweet = Tweet(id: id, username: username, screenName: screenName, text: text, createdAt: createdAt, profileImageUrlString: urlString ?? "", mediaUrlString: mediaUrlString ?? expandedUrlString, latitude: nil, longitude: nil, location: userLocation )
                    self.tweetList.append(tweet)

                        i+=1
                    }
                      self.tableView.reloadData()
                      self.activityIndicator.isHidden = true
                      self.activityIndicator.stopAnimating()
            case .failure(let error):
                    print("Request failed with error: \(error)")
                            }
                })
        }
    
    func imageViewTapped(sender: TweetTableViewCell) {
         performSegue(withIdentifier: "mediaDetail", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "mediaDetail"){
            let destinationViewController = segue.destination as! DetailMediaViewController
            destinationViewController.url = imageViewUrl
        }
    }
}

extension TweetsViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.tweetList.count
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TweetsViewController.cellId, for: indexPath) as! TweetTableViewCell
        cell.delegate = self
        imageViewUrl = cell.delegate!.imageViewUrl
       
        let tweet: Tweet!
        tweet = tweetList[indexPath.row]
        cell.update(with: tweet)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let destinationViewController = storyboard?.instantiateInitialViewController()  as? DetailMediaViewController
        self.navigationController?.pushViewController(destinationViewController!, animated: true)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        searchBar.endEditing(true)
    }
}

//Search
extension TweetsViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print(searchText)

        if searchText == "" {
            isSearching = false
        }else{
        isSearching = true
        self.searchForText = searchText
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        tweetList.removeAll()
        searchForTweets(cc: oauth1.cc, uc: oauth2.uc)
        tableView.reloadData()
        }
   }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
}

