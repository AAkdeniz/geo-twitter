//
//  DetailTableViewController.swift
//  GeoTwitter
//
//  Created by Ahmet Akdeniz on 2019-09-28.
//  Copyright © 2019 aca. All rights reserved.
//

import UIKit
import AVKit
import Alamofire
import SwiftyJSON

class DetailTableViewController: UITableViewController {
    
    @IBOutlet var detailView: UITableView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var screenName: UILabel!
    @IBOutlet weak var tweetImageView: UIImageView!
    @IBOutlet weak var tweetText: UILabel!
    @IBOutlet weak var retweetButton: UIButton!
    @IBOutlet weak var favoriteButton: UIButton!
    var tweet: Tweet?
    var tweetId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        detailView.delegate = self
        
        tableView.estimatedRowHeight = tableView.rowHeight
        tableView.rowHeight = UITableView.automaticDimension
        
        if let tweet = tweet{
            updateUI(with: tweet)
        }
    }
    
    func updateUI(with tweet: Tweet){
        userName.text = tweet.username
        screenName.text = tweet.screenName
        tweetText.text = tweet.text
        tweetId = tweet.id
           
        let profileUrl = tweet.profileImageUrlString
        if let profileImageUrl = profileUrl{
            profileImage.loadImageUsingUrlString(urlString: profileImageUrl)
            profileImage.roundedCorners()
        }
        if let tweetUrl = tweet.mediaUrlString {
            let videoLauncher = VideoLauncher()
            videoLauncher.playMedia(with: tweetUrl, tweetImageView: tweetImageView)
            tweetImageView.roundedCorners()
            }else{
            tweetImageView.image = nil
            defaultTweetImageViewHeightConstraint = tweetImageViewHeightConstraint.constant
            tweetImageViewHeightConstraint.constant = 0
            tableView.layoutIfNeeded()
        }
    }
    @IBAction func retweetButtonTapped(_ sender: Any) {
        if let tweet = tweet{
                   retweet(id: tweet.id)
               }
    }
    
    @IBAction func favoriting(_ sender: Any) {
        if let tweet = tweet{
            setFavorite(id: tweet.id)
        }}
    
    @IBAction func shareButton(_ sender: Any) {
        if let tweet = tweet {
        let share = "\nName: \(tweet.username) \nText: \(tweet.text)\n"
        let activityController = UIActivityViewController(activityItems: [share], applicationActivities: nil)
                present(activityController, animated: true, completion: nil)
        }
    }
    
    private func setFavorite(id: String){            
        let url = URL(string: "https://api.twitter.com/1.1/favorites/create.json")!
        let params = ["id":id]
     
        let timestamp = String(Int(Date().timeIntervalSince1970))
        let nonce =  UUID().uuidString
       
        let signature = OAuth.calculateSignature(url: url, method: "POST",parameter: params, timestamp: timestamp, nonce: nonce, consumerCredentials: oauth1.cc, userCredentials: oauth2.uc )
      
        let headers = ["Authorization": signature]
            
                          
    Alamofire.request(url, method:.post, parameters: params, headers: headers).responseJSON { response in switch response.result {
                case .success:
                self.favoriteButton.tintColor = .red
                case .failure(let error):
            print("Request failed with error: \(error)")
                    }
                }
    }
    
    private func retweet(id: String){
           let url = URL(string: "https://api.twitter.com/1.1/statuses/retweet/:id.json")!
           let params = ["id":id]
        
           let timestamp = String(Int(Date().timeIntervalSince1970))
           let nonce =  UUID().uuidString
          
           let signature = OAuth.calculateSignature(url: url, method: "POST",parameter: params, timestamp: timestamp, nonce: nonce, consumerCredentials: oauth1.cc, userCredentials: oauth2.uc )
         
           let headers = ["Authorization": signature]
               
                             
       Alamofire.request(url, method:.post, parameters: params, headers: headers).responseJSON { response in switch response.result {
                   case .success:
                   self.retweetButton.tintColor = .green
                   case .failure(let error):
               print("Request failed with error: \(error)")
                       }
                   }
       }
    
    @IBOutlet weak var tweetImageViewHeightConstraint: NSLayoutConstraint!
    private var defaultTweetImageViewHeightConstraint: CGFloat!

}
