//
//  MapViewController.swift
//  GeoTwitter
//
//  Created by Ahmet Akdeniz on 2019-09-28.
//  Copyright © 2019 aca. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Alamofire
import SwiftyJSON


class MapViewController: UIViewController {
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var distanceLabel: UILabel!
    
    var tweetList : [Tweet] = []
    var tweetAnnotation: TweetAnnotation?
    let locationManager = CLLocationManager()
    var selectedAnnotation: TweetAnnotation?
    // Default distance: meters
    var regionInMeters = 5000.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        
        mapView.register(TweetAnnotationView.self, forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
        
        getMapUI(cc: oauth1.cc, uc: oauth2.uc)
        
        checkLocationServices()
    }
    
    @IBAction func sliderAction(_ sender: Any) {
        if((regionInMeters/1000) != Double(slider.value)){
           
            self.mapView.removeAnnotations(self.mapView.annotations)
         
            regionInMeters = Double(slider.value)*1000
            distanceLabel.text = String(Int(regionInMeters/1000)) + " KM"
            
            centerViewOnUserLocation()
            getMapUI(cc: oauth1.cc, uc:  oauth2.uc)
        }
        
    }
    
    func setupAnnotation(tweetAnnotation: TweetAnnotation){
        
        let _annotation = TweetAnnotation(title: tweetAnnotation.title!, locationName: tweetAnnotation.locationName , discipline: tweetAnnotation.discipline, coordinate: tweetAnnotation.coordinate)
        mapView.addAnnotation(_annotation)
    }
    
    func setupLocationManager(){
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    // Hardcoded for testing emulator: Delete initialLocation later
    let initialLocation = CLLocation(latitude: 45.5834390, longitude: -73.614929)
    func centerViewOnUserLocation(){
        // User's current location and use this in real
//        if let location = locationManager.location?.coordinate{
//                   let region = MKCoordinateRegion.init(center: location, latitudinalMeters: regionInMeters, longitudinalMeters: regionInMeters)
//                   mapView.setRegion(region, animated: true)
               }
        // For emulator
    if (locationManager.location?.coordinate) != nil{
            let region = MKCoordinateRegion.init(center: initialLocation.coordinate, latitudinalMeters: regionInMeters, longitudinalMeters: regionInMeters)
            mapView.setRegion(region, animated: true)
        }
    }
    
    func checkLocationServices(){
        if CLLocationManager.locationServicesEnabled(){
           setupLocationManager()
            checkLocationAuthorization()
        }else{
            // Show Alert
        }
    }
    
    func checkLocationAuthorization(){
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            mapView.showsUserLocation = true
            centerViewOnUserLocation()
            break
        case .denied:
            break
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            break
        case .restricted:
            break
        case .authorizedAlways:
            break
        default:
            // Unexpected Situation happened
            break
        }
    }
    
    @objc func detailButtonTapped(){
            performSegue(withIdentifier: "tweetDetail", sender: self)
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        
        self.selectedAnnotation = view.annotation as? TweetAnnotation;        
    }

      override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
          if(segue.identifier == "tweetDetail"){
            let destinationViewController = segue.destination as! DetailTableViewController
        
            for tweet in self.tweetList{
                               if tweet.latitude == selectedAnnotation?.coordinate.latitude && tweet.longitude == selectedAnnotation?.coordinate.longitude {
         
            destinationViewController.tweet = tweet
            }
        }
    }
}
        private func getMapUI(cc : (String, String), uc : (String, String)){
         
                        let url = URL(string: "https://api.twitter.com/1.1/search/tweets.json")!
            let params = ["geocode":"45.583439,-73.614929,\(regionInMeters/1000)km","count":"10"]
            
            let timestamp = String(Int(Date().timeIntervalSince1970))
            let nonce =  UUID().uuidString
            let signature = OAuth.calculateSignature(url: url, method: "GET",parameter: params, timestamp: timestamp, nonce: nonce, consumerCredentials: cc, userCredentials: uc )
                        
            let headers = ["Authorization": signature]
                
                Alamofire.request(url, method:.get, parameters: params, headers: headers).responseJSON (completionHandler: { response in switch response.result {
                    case .success(let value):
                    let json = JSON(value)
                    var i = 0
                    var latitude = 0.0
                    var longitude = 0.0
                    
                    while (i < 10){
                    let data = json["statuses"]
                    let text = data[i]["text"].stringValue
                    let mediaUrlString = data[i]["extended_entities"]["media"][0]["video_info"]["variants"][1]["url"].string
                                             
                    let expandedUrlString = data[i]["extended_entities"]["media"][0]["media_url"].string
                                      
                    // Geocode is null for almost all user
                    let latitudeString = data[i]["geo"]["coordinates"][0].string
                    let longitudeString = data[i]["geo"]["coordinates"][1].string
                
                    // Normally use that condition if it has geo plus one
//                    if let latitudeString = latitudeString, let longitudeString = longitudeString {
//                        latitude = Double(latitudeString)!
//                        longitude = Double(longitudeString)!
//                        i += 1
//                    }
                            
                        if let latitudeStr = latitudeString{
                            latitude = Double(latitudeStr)!
                        }else{
                            latitude = self.generateRandomLatitude()
                        }

                         if let longitudeStr = longitudeString{
                                longitude = Double(longitudeStr)!
                         }else{
                            longitude = self.generateRandomLongitude()
                         }
                        
                    let createdAt = data[i]["created_at"].stringValue
                    let id = data[i]["id"].stringValue
                    let user = data[i]["user"]
                    let username = user["name"].stringValue
                    let screenName = user["screen_name"].stringValue
                    let profileImageUrlString = user["profile_image_url"].stringValue
                    let urlString: String = profileImageUrlString
                        
                    let userLocation = user["location"][0].stringValue
                      
                        //If geocode is null than Find geocode from a given address
                        //getGeocodeByLocationName(locationName: userLocation)
                  
                        let tweet = Tweet(id: id, username: username, screenName: screenName, text: text, createdAt: createdAt, profileImageUrlString: urlString, mediaUrlString: mediaUrlString ?? expandedUrlString, latitude: latitude, longitude: longitude, location: userLocation )
            
                    let tweetAnnotation = TweetAnnotation(title: screenName, locationName: userLocation, discipline: username, coordinate: CLLocationCoordinate2D(latitude: latitude,longitude: longitude))
                     self.setupAnnotation(tweetAnnotation: tweetAnnotation)
                    
                    self.tweetList.append(tweet)
                        i+=1
                    } case .failure(let error):
                    print("Request failed with error: \(error)")
                            }
                })
            }
    
    func getGeocodeByLocationName(locationName: String)-> CLLocationCoordinate2D{
            let geocoder = CLGeocoder()
            var geo = CLLocationCoordinate2D()
            geocoder.geocodeAddressString(locationName) { (placemarks, error) in
            if error != nil {
                //Deal with error here
                } else if let placemarks = placemarks {

            if let coordinate = placemarks.first?.location?.coordinate {
                                        geo = coordinate
    
                        }
                }
        }
        return geo
    }
    
    func generateRandomLatitude() ->Double{
        let randomLatitude = Double.random(in: (45.5834390 - 0.025)...(45.5834390 + 0.025))
        return randomLatitude
    }
    
    func generateRandomLongitude() ->Double{
        let randomLongitude = Double.random(in: (-73.614929 - 0.025)...(-73.614929 + 0.025))
           return randomLongitude
    }
}

extension MapViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else {return}
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion.init(center: center, latitudinalMeters: regionInMeters, longitudinalMeters: regionInMeters)
        mapView.setRegion(region, animated: true)
    }
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        checkLocationAuthorization()
        }
    }

// Configured the Annotation View
extension MapViewController: MKMapViewDelegate{
}
