//
//  Tweet.swift
//  GeoTwitter
//
//  Created by Ahmet Akdeniz on 2019-09-30.
//  Copyright © 2019 aca. All rights reserved.
//

import UIKit

struct Tweet {
    let id: String
    let username: String
    let screenName: String
    let text: String
    let createdAt: String?
    let profileImageUrlString : String?
    let mediaUrlString: String?
    let latitude: Double?
    let longitude: Double?
    let location: String?
}
