//
//  GeoTwitterUnitTests.swift
//  GeoTwitterUnitTests
//
//  Created by Ahmet Akdeniz on 2019-10-10.
//  Copyright © 2019 aca. All rights reserved.
//

import XCTest
@testable import GeoTwitter

class GeoTwitterUnitTests: XCTestCase {
    // Added basic test: compare to tweet and tweetViewModel
    func testTweetAnnotationToTweet(){
        let tweet = Tweet(id: "0", username: "@akdeniz", screenName: "ahmet", text: "demo text", createdAt: "", profileImageUrlString: "", mediaUrlString: "", latitude: nil, longitude: nil, location: "Montreal")
        let tweetViewModel = TweetViewModel(tweet: tweet)
        XCTAssertEqual(tweet.username, tweetViewModel.username)
    }
}
